﻿using System;

namespace Task2
{
    public class NumberParser : INumberParser
    {
        public int Parse(string stringValue)
        {
            long number = 0;
            int index = 0;
            bool isNegative = false;

            stringValue = stringValue?.Trim();

            if (stringValue == null)
            {
                throw new ArgumentNullException();
            }

            if (stringValue.Length == 0)
            {
                throw new FormatException();
            }

            if (stringValue[0] == '-')
            {
                isNegative = true;
                index++;
            }
            else if (stringValue[0] == '+')
            {
                index++;
            }

            while (index < stringValue.Length)
            {
                if (stringValue[index] < '0' || stringValue[index] > '9')
                {
                    throw new FormatException();
                }

                number = number * 10;
                number += (stringValue[index] - '0');
                index++;
            }

            if (isNegative)
            {
                number = -number;
            }

            if (number < int.MinValue ||  number > int.MaxValue)
            {
                throw new OverflowException("Input number is out of int32 range.");
            }

            return (int)number;
        }
    }
}