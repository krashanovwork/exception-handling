﻿using System;

namespace Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var inputLine = Console.ReadLine();

                Console.WriteLine($"First character of input line is '{inputLine[0]}'");
            }
            catch (Exception)
            {

                Console.WriteLine("Input string cannot be empty");
            }
        }
    }
}